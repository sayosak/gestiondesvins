package com.example.tp2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;

public class WineActivity extends AppCompatActivity implements Serializable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        //Get intent
        Intent intent = getIntent();
        final Wine clickedWine =(Wine) intent.getParcelableExtra("wine");
        final boolean add = (boolean) intent.getExtras().getBoolean("add");

        //SetUp layout elements
        final EditText title = findViewById(R.id.titleWine);
        final TextView region = findViewById(R.id.region);
        final TextView loc = findViewById(R.id.loc);
        final TextView climat = findViewById(R.id.climat);
        final TextView plantArea = findViewById(R.id.plantArea);

        String name = clickedWine.getTitle();
        String regionStr = clickedWine.getRegion();
        String locStr = clickedWine.getLocalization();
        String climatStr = clickedWine.getClimate();
        String plantAreaStr = clickedWine.getPlantedArea();

        title.setText(name);
        region.setText(regionStr);
        loc.setText(locStr);
        climat.setText(climatStr);
        plantArea.setText(plantAreaStr);


        //button sauvegarder
        final Button btnSave = findViewById(R.id.btnSauvegarder);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameStr = title.getText().toString();
                String regionStr = region.getText().toString();
                String locStr = loc.getText().toString();
                String climatStr = climat.getText().toString();
                String plantAreaStr = plantArea.getText().toString();

                clickedWine.updateWine(nameStr,regionStr,locStr,climatStr,plantAreaStr);

                if (nameStr.isEmpty()){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                    alertDialogBuilder.setTitle("Sauvegarde impossible");
                    alertDialogBuilder.setMessage("Le nom du vin doit être non vide.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }else{
                    //send back an intent
                    Intent intent = new Intent(WineActivity.this, MainActivityDb.class);
                    intent.putExtra("wine", (Parcelable) clickedWine);
                    if(add){
                        intent.putExtra("add", true);
                    }else{
                        intent.putExtra("add", false);
                    }
                    setResult(1,intent);
                    finish();
                }
            }
        });




    }
}
