package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";
    public static long ID = 1;

    public static boolean ifPopulate = false;


    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE cellar (_id NUMERIC, name TEXT, region TEXT, localization TEXT, climate TEXT, publisher TEXT, UNIQUE (name,region) ON CONFLICT ROLLBACK) ;");
        ifPopulate = true;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME); // drops the old database
        onCreate(db); // run onCreate to get new database
    }

    /**
     * Adds a new wine
     *
     * @return true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        wine.setId(ID);

        ContentValues cv = new ContentValues();
        cv.put(_ID, wine.getId());
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        // Inserting Row
        long rowID;
        // call db.insert()

        try{
            rowID =db.insertOrThrow(TABLE_NAME,null,cv);
            ID++;
        }catch(SQLiteConstraintException e){
            rowID = -1;
            System.out.println(e.getMessage());
        }
        // nullColumnHack
        //db.insert(TABLE_NAME,null,cv);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        String selection = WineDbHelper._ID + " == ?";
        String[] selectionArgs = { Long.toString(wine.getId())};

        int res = 0;
        try {
            res = db.update(WineDbHelper.TABLE_NAME,
                    cv,
                    selection,
                    selectionArgs);
        }catch(SQLiteConstraintException e){
            System.out.println(e.getMessage());
        }

        // updating row
	    // call db.update()
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        // call db.query()
        Cursor cursor = db.query(TABLE_NAME,
                new String[] { "ROWID AS _id", COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA},
                null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        Wine wine = cursorToWine(cursor);
        String selection = WineDbHelper._ID + " == ?";
        String[] selectionArgs = { Long.toString(wine.getId())};
        db.delete(WineDbHelper.TABLE_NAME, selection, selectionArgs);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));


        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        // build a Wine object from cursor
        long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(WineDbHelper._ID)));
        String title = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION ));
        String localization = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_CLIMATE));
        String plantedArea = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA));

        Wine wine = new Wine(id,title, region, localization, climate, plantedArea);
        return wine;
    }

    public void populateOnlyOneTime(SQLiteDatabase db){
        if (ifPopulate){
            populate();
        }
    }
}

