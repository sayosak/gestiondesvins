package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


public class MainActivityDb extends AppCompatActivity {
    public  WineDbHelper wDbH;
    public  SimpleCursorAdapter adapter;
    public ListView wineList;
    private String whereClause = WineDbHelper.COLUMN_NAME+ "=?";

    private String[] allCulumns = new String[] { WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_db);
        wDbH = new WineDbHelper(this);
        SQLiteDatabase db = wDbH.getReadableDatabase();
        wDbH.populateOnlyOneTime(db);
        final Cursor cursor = wDbH.fetchAllWines();

        adapter = new SimpleCursorAdapter(this,
                        android.R.layout.simple_list_item_2, cursor,
                        new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_LOC},
                        new int[] { android.R.id.text1, android.R.id.text2}, 0);

        wineList = (ListView) findViewById(R.id.listWineDb);
        wineList.setAdapter(adapter);
        db.close();

        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Cursor cursorWine =  (Cursor) parent.getItemAtPosition(position);

                Wine clickedWine = WineDbHelper.cursorToWine(cursorWine);

                Intent intent = new Intent(MainActivityDb.this, WineActivity.class);
                intent.putExtra("wine", (Parcelable) clickedWine);
                intent.putExtra("add",false);
                startActivityForResult(intent,1);

            }
        });

        //add menu to the wineList
        registerForContextMenu(wineList);
        wineList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MainActivityDb.super.onCreateContextMenu(menu, v, menuInfo);
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.delete_wine, menu);
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Wine wineToAdd = new Wine();
                Intent intent = new Intent(MainActivityDb.this, WineActivity.class);
                intent.putExtra("wine", (Parcelable) wineToAdd);
                intent.putExtra("add",true);
                startActivityForResult(intent,1);

            }
        });
    }


    /**
     * Manage the data when we back to the main activity
     * @param requestCode
     * @param resultCode
     * @param dataIntent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataIntent){
        super.onActivityResult(requestCode,resultCode,dataIntent);
        if (dataIntent != null){
            Wine wine = dataIntent.getParcelableExtra("wine");
            boolean add = (dataIntent.getExtras()).getBoolean("add");
            assert wine != null;
            if(add){
                boolean insert = wDbH.addWine(wine);

                if (insert){
                    // change the cursor
                    adapter.changeCursor(wDbH.getReadableDatabase().query(WineDbHelper.TABLE_NAME,
                            allCulumns,
                            null, null, null, null, null));

                    //refresh the listView
                    adapter.notifyDataSetChanged();

                    mt("Un Vin par défaut a été ajouté.");
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivityDb.this);
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
            }else{
                int res = wDbH.updateWine(wine);
                if (res != 1){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivityDb.this);
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }
            }
        }
        // change the cursor
        adapter.changeCursor(wDbH.getReadableDatabase().query(WineDbHelper.TABLE_NAME,
                allCulumns,
                null, null, null, null, null));

        //refresh the listView
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume(){
        super.onResume();
        mt("Resume");
    }

    public void mt(String string){
        Toast.makeText(this,string,Toast.LENGTH_SHORT).show();
    }

    /**
     * Manage the menu items
     * @param item
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.delete:

                Cursor cursorWine =(Cursor) wineList.getItemAtPosition(info.position);
                wDbH.deleteWine(cursorWine);

                // change the cursor
                adapter.changeCursor(wDbH.getReadableDatabase().query(WineDbHelper.TABLE_NAME,
                        allCulumns,
                        null, null, null, null, null));

                //refresh the listView
                adapter.notifyDataSetChanged();
                mt("Un Vin a été supprimé.");

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
